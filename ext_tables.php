<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

/**
 * Register icons
 */
$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);

$iconRegistry->registerIcon(
    'tx-calendar-plugin-list-view',
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    ['source' => 'EXT:calendar_frontend/Resources/Public/Icons/plugin_listview.svg']
);
