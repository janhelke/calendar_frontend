document.addEventListener("DOMContentLoaded", function () {
  var calendarEl = document.getElementById("calendar");

  var calendar = new FullCalendar.Calendar(calendarEl, {
    events: "' . $apiCall . '",

    plugins: ["dayGrid", "timeGrid"],
    header: {
      left: "dayGridMonth,timeGridWeek,timeGridDay custom1",

    },
  });

  calendar.render();
});
