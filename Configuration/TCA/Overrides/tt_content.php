<?php
if (!defined('TYPO3')) {
    die('Access denied.');
}

use TYPO3\CMS\Extbase\Utility\ExtensionUtility;

(static function () {
    ExtensionUtility::registerPlugin(
        'CalendarFrontend',
        'ListView',
        'LLL:EXT:calendar_frontend/Resources/Private/Language/locallang_db.xlf:plugin.list_view',
        'tx-calendar-plugin-list-view'
    );

    ExtensionUtility::registerPlugin(
        'CalendarFrontend',
        'fullCalendar',
        'LLL:EXT:calendar_frontend/Resources/Private/Language/locallang_db.xlf:plugin.full_calendar',
        'tx-calendar-plugin-full-calendar'
    );
})();
