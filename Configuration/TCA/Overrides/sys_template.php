<?php
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

if (!defined('TYPO3')) {
    die('Access denied.');
}

(static function () {
    /**
     * Extension key
     */
    $extensionKey = 'calendar_frontend';

    /**
     * Default TypoScript
     */
    ExtensionManagementUtility::addStaticFile(
        $extensionKey,
        'Configuration/TypoScript',
        'LLL:EXT:calendar_frontend/Resources/Private/Language/locallang_db.xlf:typoscript.template_name'
    );
})();
