<?php
declare(strict_types=1);

namespace JanHelke\CalendarFrontend\Controller;

use JanHelke\CalendarApiClient\Service\ApiService;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

class FullCalendarController extends ActionController
{
    public function indexAction(): ResponseInterface
    {
        $rangeStart = new \DateTime('-1 month');
        $rangeEnd = new \DateTime('+1 month');
        $apiCall = (new ApiService())->call(
            'entry',
            [
                'rangeStart' => $rangeStart->format('c'),
                'rangeEnd' => $rangeEnd->format('c'),
            ]
        );

        $typo3Version = VersionNumberUtility::convertVersionNumberToInteger(VersionNumberUtility::getCurrentTypo3Version() ?? 'TYPO3_version');
        $pageRenderer = $typo3Version <= 9_005_000 ? $this->objectManager->get(PageRenderer::class) : new PageRenderer();

        $pageRenderer->addJsInlineCode(
            'jscalendar',
            '
document.addEventListener("DOMContentLoaded", function() {
    var calendarEl = document.getElementById("calendar");

    var calendar = new FullCalendar.Calendar(calendarEl, {
    events: "' . $apiCall . '",

        plugins: [ "dayGrid", "timeGrid" ],
              header: {
        left: "dayGridMonth,timeGridWeek,timeGridDay custom1",

      },
    });

    calendar.render();
});
'
        );
        return $this->htmlResponse();
    }
}
