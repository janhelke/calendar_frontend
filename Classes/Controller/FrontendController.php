<?php
declare(strict_types=1);

namespace JanHelke\CalendarFrontend\Controller;

use JanHelke\CalendarApiClient\Service\ApiService;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/**
 * Calender Frontend Controller
 */
class FrontendController extends ActionController
{
    public function listAction(): ResponseInterface
    {
        $rangeStart = new \DateTime();
        $rangeEnd = new \DateTime('+1 year');
        $apiCall = (new ApiService())->call(
            'entry',
            [
                'rangeStart' => $rangeStart->format('c'),
                'rangeEnd' => $rangeEnd->format('c'),
            ]
        );

        $jsonResponse = file_get_contents($apiCall);
        $this->view->assign('entries', json_decode($jsonResponse, true));
        return $this->htmlResponse();
    }

    public function showAction(): ResponseInterface
    {
        $apiCall = (new ApiService())->call(
            'entry',
            [
                'event' => (int)$this->request->getArgument('event'),
            ]
        );

        $jsonResponse = file_get_contents($apiCall);
        $this->view->assign('event', json_decode($jsonResponse, true));
        return $this->htmlResponse();
    }
}
