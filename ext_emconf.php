<?php

$EM_CONF['calendar_frontend'] = [
    'title' => 'Calendar Frontend',
    'description' => 'A basic implementation of a working frontend for ext:cal to uncouple the calendar FRONTEND from the view.',
    'category' => 'plugin',
    'version' => '1.0.0',
    'state' => 'stable',
    'author' => 'Jan Helke',
    'author_email' => 'calendar@typo3.helke.de',
    'constraints' => [
        'depends' => [
            'calendar_api' => '1.0 - 1.999'
        ],
    ]
];
