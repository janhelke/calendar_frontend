# TYPO3 Extension calendar_frontend
This extension provides a fully functional frontend view for the calendar_framework. This frontend view is heavily based on version 4.3 of the FullCalender provided by fullcalender.io

NOTE: Please keep in mind, that this extension is, even if fully functional, is a rough implementation to have a working frontend at hand. If you have the need to implement a custom HTML calendar element, you are on your own. Maybe at some time we'll find a gracious frontend developer who wants to implement something like this.

## Installation
Just install the extension and include the provided TypoScript. Now you'll find a new plugin called FullCalendar in the list of available plugins.

Currently the calendar is only initialized using <script> tags. Using the ES6 build system might be a task for the future.

###External libraries vs. local libraries
For the majority of users, loading the JavaScript and CSS files for the calendar on the fly from a CDN might be sufficient. If for some reasons you rely only on local libs (firewall settings, distrust) you can download the version 4.3 of FullCalendar and change the following TypoScript lines to point to your local sources.

    page {
      includeJSFooterlibs {
        calendar_frontend_core = https://unpkg.com/@fullcalendar/core@4.3/main.js
        calendar_frontend_core.external = 1
        calendar_frontend_daygrid = https://unpkg.com/@fullcalendar/daygrid@4.3/main.js
        calendar_frontend_daygrid.external = 1
      }
      includeCSSLibs {
        calendar_frontend_core = https://unpkg.com/@fullcalendar/core@4.3/main.css
        calendar_frontend_core.external = 1
        calendar_frontend_daygrid = https://unpkg.com/@fullcalendar/daygrid@4.3/main.css
        calendar_frontend_daygrid.external = 1
      }
    }

