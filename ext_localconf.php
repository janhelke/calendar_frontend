<?php

(static function () {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'CalendarFrontend',
        'ListView',
        [
            \JanHelke\CalendarFrontend\Controller\FrontendController::class => 'list, show',
        ],
        []
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'CalendarFrontend',
        'fullCalendar',
        [
            \JanHelke\CalendarFrontend\Controller\FullCalendarController::class => 'index',
        ],
        []
    );
})();
